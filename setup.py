#!/usr/bin/env python
# -*- coding: utf-8 -*-
import usermgmt


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


version = usermgmt.__version__


setup(
    name='django-usermgmt',
    version=version,
    description="""a what.digital package that enhances and configures django.contrib.auth and provides a custom User model""",
    long_description="",
    author='Mario Colombo',
    author_email='mario@what.digital',
    url='https://gitlab.com/what-digital/django-usermgmt',
    packages=[
        'usermgmt',
    ],
    include_package_data=True,
    install_requires=[
        'django>=1.11',
        'django-cors-headers',
        'djangorestframework',
        'social-auth-app-django',
        'rest-social-auth',
        'oauth2client',
        'dry-rest-permissions',
        'django-import-export',
    ],
    license="BSD",
    zip_safe=False,
    keywords='django-usermgmt',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
