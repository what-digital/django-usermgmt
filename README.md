
# django-usermgmt


The biggest achievement of this app is that it does away with the username. The email address is used instead for auth.

## Develop django-usermgmt

### Setup

- `cd` into the `django-usermgmt` project and run `python setup.py develop`, this allows to run the test project's manage.py
- ./usermgmt_test_project/manage.py makemigrations should now work


## Setup


add `usermgmt` to your settings.py `INSTALLED_APPS` after your custom_user application:

    INSTALLED_APPS = [
        'usermgmt',
        'src.custom_user',
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    ]

Make sure you add this to the project at the beginning of development. In Django, the user model cannot be changed afterwards.

Please see https://docs.djangoproject.com/en/2.0/topics/auth/customizing/#substituting-a-custom-user-model

add the following to your site's urlpatterns in urls.py:

    # standard Django auth urls
    url(r'^/', include('django.contrib.auth.urls')),
    # custom wrapping functionality
    url(r'^accounts/', include('usermgmt.urls', namespace='accounts')),
    
django>2:

    # standard Django auth urls
    path(r'', include('django.contrib.auth.urls')),
    # custom wrapping functionality
    path(r'accounts/', include('usermgmt.urls', namespace='accounts')),
    

Add the following to your settings:

    USER_FIELDS = ['email']
    AUTH_USER_MODEL = "custom_user.User"
    LOGIN_REDIRECT_URL = ""

    # this is the name for the login page from urls.py
    LOGIN_URL = 'accounts:login'

    # Make PSA render its exceptions instead of throwing them
    LOGIN_ERROR_URL = 'accounts:login'

    VALIDATE_EMAIL_REDIRECT_URL = "accounts:password_set"
    
Last but not least override `registration/base.html` in your project's main template folder to integrate it into your designs. Dont override it in the custom_user's template folder as custom_user app is after usermgmt app in INSTALLED_APPS and the override will not be picked up by django. 

## Django Admin

Register the following or create your own:

- admin.site.register(UserValidationCode, UserValidationCodeAdmin)
- admin.site.register(User, UserAdmin)

## API

    INSTALLED_APPS = [
        'src.custom_user',
        'usermgmt',
     ...
     
        'rest_framework',
    
then: 

    from usermgmt.api import routers
    from usermgmt.api.urls import router as usermgmt_router
    
    # Rest Framework
    router = routers.DefaultRouter()
    router.extend(usermgmt_router)


    urlpatterns = [
        path('accounts/api/', include(router.urls)),
        ...
    ...


## Features

* tbd
* tbd

## Documentation

TBD

## Contributing

TBD
