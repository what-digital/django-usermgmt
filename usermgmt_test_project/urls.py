from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from usermgmt.urls import urlpatterns as usermgmt_url_patterns


urlpatterns = usermgmt_url_patterns

urlpatterns += i18n_patterns(

)
