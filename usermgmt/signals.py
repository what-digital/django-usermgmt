import django.dispatch

user_activated = django.dispatch.Signal(providing_args=[
    "user",
])
