# -*- coding: UTF-8 -*-

import logging
from django.shortcuts import render
from django.conf import settings
from django.conf import settings
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login)
from django.contrib.auth.decorators import login_required

from usermgmt.models import UserValidationCode
from usermgmt import signup_form
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as ugettext

from usermgmt.signals import user_activated

# Create your views here.

# Get an instance of a logger
logger = logging.getLogger(__name__)


def login(request):
    template_name = "registration/login.html"

    if request.GET.get('next') == reverse('accounts:login') or request.GET.get('next') == reverse('accounts:signup'):
        # if next points to signup and the user comes here to log in he wont be logged in for some reason.
        if not request.GET._mutable:
            request.GET._mutable = True

        request.GET['next'] = ""

    if request.user.is_authenticated:
        if request.GET.get('next'):
            return redirect(request.GET.get('next'))
        else:
            return redirect(settings.LOGIN_REDIRECT_URL)

    if request.GET.get('deeplink'):
        message = _("Welcome! You must sign in at this point and will then be redirected to the requested page.")
        messages.add_message(request, messages.SUCCESS, message)
        logger.debug(message)

    return LoginView.as_view(
        template_name=template_name,
        extra_context={
            'button_action': _('Log in with') + " ",
        },
    )(request)


@login_required
def profile(request):
    context = {
        'button_action': _('Connect '),
        'password_set': request.user.has_usable_password(),
    }
    return render(request, 'registration/profile.html', context)


def signup(request):
    if request.GET.get('deeplink'):
        message = _("You will be redirected to the requested page after signup.")
        messages.add_message(request, messages.SUCCESS, message)
        logger.debug(message)

    if request.user.is_authenticated:
        # we wanna log the person out first
        logout(request)

    # This shows the signup form and displays errors
    if request.method == 'POST':
        next = request.POST.get('next')
        # This form has been filled in by the user
        form = signup_form.ExtendedUserCreationForm(request.POST)
        if form.is_valid():
            newuser = form.save()
            logger.debug('Created user ' + newuser.email + "& sent email validation")
            # the user needs to undergo email validation first, so we want to create him inactive

            # if the user has been redirected to signup, try to route him back to the page he originally requested.
            newuser.send_validation_mail(next_url=request.POST.get('next'))
            # Set a message and redirect to login
            message = _("We have sent an activation mail to " + newuser.email + " . Please go have a look at your inbox")
            messages.add_message(request, messages.SUCCESS, message)
            logger.debug(message)
            return redirect('accounts:login')

    else:
        next = request.GET.get('next')
        # initial, empty form
        form = signup_form.ExtendedUserCreationForm()

    context = {
        'button_action': _('Sign up with') + " ",
        'form': form,
        'next': next,
    }

    return render(request, 'registration/signup.html', context)


def validate_email(request, code):
    if request.user.is_authenticated:
        # we wanna log the person out first
        logout(request)
        request.session.flush()

    # This view sets the user to active and logs the user in or renders an error page

    try:
        user_code_combination = UserValidationCode.objects.get(hash=code)
        user = user_code_combination.user
        user.is_active = True
        user.save()
        user_code_combination.delete()
        user_activated.send(sender=None, user=user)
        # Crazy hack: http://stackoverflow.com/questions/15192808/django-automatic-login-after-user-registration-1-4
        user.backend = "django.contrib.auth.backends.ModelBackend"
        auth_login(request, user)
        logger.debug('Validation succeeded - logged in ' + user.email + " - is_authenticated: " + str(
            request.user.is_authenticated))
        # the user will get next urls in the e-mail link if they came through a deep url (like stack sharing).
        if request.GET.get('next'):
            return redirect(request.GET.get('next'))
        else:
            return redirect(settings.VALIDATE_EMAIL_REDIRECT_URL)

    except UserValidationCode.DoesNotExist:
        message = _("This activation code is not valid anymore")
        messages.add_message(request, messages.SUCCESS, message)
        logger.debug(message)
        # Send the user to signup
        return redirect('accounts:signup')


from django.contrib.auth.forms import SetPasswordForm


@login_required()
def set_password(request):
    # This shows the signup form and displays errors
    if request.method == 'POST':
        # This form has been filled in by the user
        form = SetPasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            logger.debug('Set password for ' + request.user.email)
            # Set a message and redirect

            # Django logs the user out after successful password set :( WTF
            # Crazy hack: http://stackoverflow.com/questions/15192808/django-automatic-login-after-user-registration-1-4
            request.user.backend = "django.contrib.auth.backends.ModelBackend"
            auth_login(request, request.user)
            message = _("Your password has been set!")
            messages.add_message(request, messages.SUCCESS, message)
            logger.debug(message)
            return redirect(settings.SET_PASSWORD_REDIRECT_URL)

    else:
        # initial, empty form
        form = SetPasswordForm(user=request.user)

    context = {
        'form': form,
    }
    return render(request, 'registration/password_set_form.html', context)
