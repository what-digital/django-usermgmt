

def activate_user(backend, user, response, *args, **kwargs):
    """
    Its possible that a user with such email address already exists, however is in inactive state
    because the user signed up via manual signup, but didnt validate the email address
    """
    if not user.is_active:
        user.is_active = True
        user.save()
