from django.db import IntegrityError
from django.test import TestCase

from usermgmt.models import User


class UserCreationTest(TestCase):
    def test_email_uniqueness_is_case_insensitive(self):
        email = 'test@localhost'
        email_same = email.upper()
        User.objects.create(email=email, password=email)
        with self.assertRaises(IntegrityError):
            User.objects.create(email=email_same, password=email_same)
