from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

import getpass
from django.contrib.auth.password_validation import validate_password

import sys

from django.core import exceptions
from django.utils.encoding import force_str

from django.db.utils import IntegrityError


class NotRunningInTTYException(Exception):
    pass


class Command(BaseCommand):
    help = 'Create a Django super user. Arguments:  password and e-mail. Command Line or programmatic.'

    requires_migrations_checks = True

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.email_field = get_user_model()._meta.get_field("email")

    def add_arguments(self, parser):
        parser.add_argument('--email', action='store', dest='email')
        parser.add_argument('--password', action='store', dest='password')
        parser.add_argument(
            '--noinput', '--no-input',
            action='store_false', dest='interactive', default=True,
            help='Tells Django to NOT prompt the user for input of any kind. ',
        )

    def execute(self, *args, **options):
        self.stdin = options.get('stdin', sys.stdin)  # Used for testing
        return super(Command, self).execute(*args, **options)

    def handle(self, *args, **options):

        def notify(msg, error=False):
            if error:
                self.stderr.write(msg)
                sys.exit(-1)
            else:
                self.stdout.write(msg)

        password = None
        email = None
        user_data = {}

        # Do quick and dirty validation if --noinput
        if not options['interactive']:
            try:
                email = options.get('email')
                password = options.get('password')

            except Exception as e:
                raise CommandError(e)

        else:
            # Prompt for email/password
            # Enclose this whole thing in a try/except to catch
            # KeyboardInterrupt and exit gracefully.
            try:
                if hasattr(self.stdin, 'isatty') and not self.stdin.isatty():
                    raise NotRunningInTTYException("Not running in a TTY")

                while email is None:
                    email = self.get_input_data(self.email_field, "Email:")

                # Get a password
                while password is None:
                    password = getpass.getpass()
                    password2 = getpass.getpass(force_str('Password (again): '))
                    if password != password2:
                        notify("Error: Your passwords didn't match.", error=True)
                        password = None
                        # Don't validate passwords that don't match.
                        continue

                    if password.strip() == '':
                        notify("Error: Blank passwords aren't allowed.", error=True)
                        password = None
                        # Don't validate blank passwords.
                        continue

            except KeyboardInterrupt:
                notify("\nOperation cancelled.", error=True)

            except NotRunningInTTYException:
                notify(
                    "Superuser creation skipped due to not running in a TTY. "
                    "You can run `manage.py createsuperuser` in your project "
                    "to create one manually."
                )

        if password:
            try:
                validate_password(password, get_user_model()({'email': email}))
            except exceptions.ValidationError as err:
                notify('\n'.join(err.messages), error=True)
        else:
            notify("please specify a password", error=True)

        if email:
            email = self.email_field.clean(email, None)
            user_data['email'] = email
            user_data['password'] = password

            try:
                get_user_model().objects.create_superuser(email=email, password=password)
            except IntegrityError:
                # if the user already exists lets update his password instead
                usr = get_user_model().objects.get(email=email)
                usr.set_password(password)
                usr.save()
                # don't exit with an error code to not throw off deploy scripts
                notify("The user already existed - password updated.")
            else:
                notify("Superuser created successfully.")
        else:
            notify('please specify an email address', error=True)

    def get_input_data(self, field, message):
        """
        Override this method if you want to customize data inputs or
        validation exceptions.
        """
        raw_value = input(message)

        try:
            val = field.clean(raw_value, None)
        except exceptions.ValidationError as e:
            self.stderr.write("Error: %s" % '; '.join(e.messages))
            val = None

        return val
