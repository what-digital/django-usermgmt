from django.urls import reverse
from django.http import HttpResponseRedirect
from urllib.parse import urlencode



def redirect_with_params(url_name, *args, **kwargs):
    """
    This helps
    :param url_name: will be resolved by reverse
    :param args:
    :param kwargs:
    :return: the full url
    """

    url = reverse(url_name, args=args)
    params = urlencode(kwargs)

    return HttpResponseRedirect(url + "?%s" % params)

