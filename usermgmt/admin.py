from django.contrib import admin
from .models import UserValidationCode, User
from django.contrib.auth import admin as auth_admin

from import_export.admin import ExportActionModelAdmin
from import_export import fields, resources

from django.utils.translation import ugettext_lazy as _

# Register your models here.


# Manage cards
class UserValidationCodeAdmin(admin.ModelAdmin):
    list_display = ('get_user_email', 'hash')
    search_fields = ['user__email']


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)
        export_order = fields


class UserAdmin(auth_admin.UserAdmin, ExportActionModelAdmin):

    # Export
    resource_class = UserResource

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name',
            'last_name',
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'is_superuser', 'is_active',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'first_name', 'last_name',)
    ordering = ('email',)



