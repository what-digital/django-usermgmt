from django.conf.urls import url, include

from usermgmt import views
from django.contrib.auth.views import logout_then_login


app_name = 'accounts'

# /accounts/...
urlpatterns = [
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'^login/$', views.login, name='login'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^validate-email/(?P<code>.*)/$', views.validate_email, name='validate_email'),
    url(r'^set-password/$', views.set_password, name='password_set'),
]
