from .views import *

# Rest Framework
from rest_framework import routers

router = routers.SimpleRouter()
# order matters!
router.register(
    r'users/reset-password-complete', UserResetPasswordCompleteViewSet,
    basename='users_reset_password_complete'
)
router.register(
    r'users/reset-password-initiate', UserResetPasswordInitiateViewSet,
    basename='users_reset_password_initiate'
)
router.register(r'users/set-password', UserSetPasswordViewSet, basename='users_set_password')
router.register(r'users/activate', UserActivationViewSet, basename='users_activate')
router.register(r'users/login', ObtainAuthTokenViewSet, basename='users_login')
router.register(r'users/signup', UserSignupViewSet, basename='users_signup')
router.register(r'users/me', UserMeDetailViewSet, basename='users_me')
router.register(r'users', UserViewSet, basename='users')
router.register(r'groups', GroupViewSet, basename='groups')
