from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_framework import serializers, mixins, viewsets
from django.utils.translation import gettext as _
from usermgmt.models import UserValidationCode
from ..signals import user_activated


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'pk',
            'first_name',
            'last_name',
            'email',
            "is_superuser",
            "is_staff",
            "is_active",
            "date_joined",
            "last_login",
        )


class UserSignupSerializer(serializers.ModelSerializer):
    password = serializers.CharField(label="Password", style={'input_type': 'password'})

    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'last_name',
            'email',
            'password',
        )
        extra_kwargs = {
            # allow_null is required https://github.com/encode/django-rest-framework/issues/4538
            'first_name': {
                'required': True,
                'allow_null': False,
                'allow_blank': False,
            },
            'last_name': {
                'required': True,
                'allow_null': False,
                'allow_blank': False,
            },
            'email': {
                'required': True,
                'allow_null': False,
                'allow_blank': False,
            },
            'password': {
                'required': True,
                'allow_null': False,
                'allow_blank': False,
            },
        }

    def create(self, validated_data):
        validated_data['is_active'] = False
        base_url_override = self.context.get('request').GET.get('base_url_override')
        password = validated_data.pop('password')
        created_user = get_user_model().objects.create(**validated_data)
        created_user.set_password(password)
        created_user.save()
        created_user.send_validation_mail(base_url_override=base_url_override)
        return created_user

    def to_representation(self, obj):
        # dont return password to browseable API after creation of the user
        representation = super().to_representation(obj)
        representation.pop('password')
        return representation


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            'name',
        )


class UserActivationSerializer(serializers.Serializer):
    """
    Activates the user with the UserValidationCode hash
    """

    hash = serializers.CharField(max_length=200)

    def create(self, request, *args, **kwargs):
        try:
            user_code_combination = UserValidationCode.objects.get(hash=request.get('hash'))
            user = user_code_combination.user
            user.is_active = True
            user.save()
            user_code_combination.delete()
            # create a hook for other applications to plug in
            user_activated.send(sender=self.__class__, user=user)
        except UserValidationCode.DoesNotExist:
            raise serializers.ValidationError(
                _('The user does either not exist or has already validated the email address.')
            )

        return user_code_combination


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class PasswordTokenSerializer(serializers.Serializer):
    password = serializers.CharField(label="Password", style={'input_type': 'password'})
    uidb64 = serializers.CharField(label="uidb64", )
    token = serializers.CharField()


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True, label="Password", style={'input_type': 'password'})
