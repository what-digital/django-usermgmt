from django.contrib.auth import get_user_model
from rest_framework.response import Response

from ..serializers import PasswordSerializer
from rest_framework import viewsets, permissions, mixins, status


class UserSetPasswordViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    api endpoint that shows the current user
    """
    serializer_class = PasswordSerializer
    permission_classes = (permissions.IsAuthenticated, )
    queryset = get_user_model().objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        user = self.request.user
        password = serializer.data.get('password')
        # TODO: make DRF respect the password policies in settings.AUTH_PASSWORD_VALIDATORS
        user.set_password(password)

        return Response({
            _('Password has been set/changed')
        }, status=status.HTTP_201_CREATED, headers=headers)



