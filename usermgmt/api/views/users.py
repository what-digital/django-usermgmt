from django.contrib.auth import get_user_model
from ..serializers import UserSerializer
from dry_rest_permissions.generics import DRYPermissionFiltersBase
from rest_framework import filters
from dry_rest_permissions.generics import DRYPermissions
from rest_framework import viewsets, permissions


class UserFilterBackend(DRYPermissionFiltersBase):
    """
    for permission in list views
    """

    def filter_list_queryset(self, request, queryset, view):
        """
        Limits all list requests to only be seen by the owners or creators.
        """
        if request.user.is_superuser_or_staff():
            return queryset.all()

        try:
            return queryset.filter(pk=request.user.pk)
        except TypeError:
            return []


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    filter_backends = (UserFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    permission_classes = (permissions.IsAuthenticated, DRYPermissions)
