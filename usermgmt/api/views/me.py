from django.contrib.auth import get_user_model

from ..serializers import UserSerializer
from dry_rest_permissions.generics import DRYPermissions
from rest_framework import viewsets, permissions


class UserMeDetailViewSet(viewsets.ReadOnlyModelViewSet):
    """
    api endpoint that shows the current user
    """
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, DRYPermissions)

    def get_queryset(self):
        return get_user_model().objects.filter(pk=self.request.user.pk)
