from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm
from django.core.exceptions import ValidationError
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext as _
from rest_framework.response import Response

from ..serializers import EmailSerializer, PasswordTokenSerializer
from rest_framework import viewsets, permissions, mixins, status
from django.contrib.auth.tokens import default_token_generator


class UserResetPasswordCompleteViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    api endpoint to change the password with a password reset token and base64 encoded user id
    """
    serializer_class = PasswordTokenSerializer
    permission_classes = (permissions.AllowAny,)

    queryset = get_user_model().objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        password = serializer.data.get('password')
        email_reset_token = serializer.data.get('token')
        uidb64 = serializer.data.get('uidb64')

        user = self.get_user(uidb64)
        if default_token_generator.check_token(user, email_reset_token):
            user.set_password(password)
            user.save()
            return Response({
                "status": "success",
                "message": _('Password changed successfully.')
            },
                status=status.HTTP_200_OK,
                headers=headers
            )

        return Response({
            "status": "error",
            "message": _('The token you sent did not check out.'),
        },
            status=status.HTTP_404_NOT_FOUND,
            headers=headers
        )

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = get_user_model()._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, get_user_model().DoesNotExist, ValidationError):
            user = None
        return user


class UserResetPasswordInitiateViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    api endpoint to trigger the password reset email
    you can use the GET parameter 'base_url_override' to specify the base url (protocol and domain) of the validation
    link in the email, please add a slash at the end of the url, example: ?base_url_override=https://www.domain.com/
    """
    serializer_class = EmailSerializer
    permission_classes = (permissions.AllowAny,)

    queryset = get_user_model().objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        email = serializer.data.get('email')
        base_url_override = request.GET.get('base_url_override')

        base_url = settings.BASE_URL
        if base_url_override:
            base_url = base_url_override

        # this is how django does it the
        form = PasswordResetForm({
            'email': email,
        })

        if form.is_valid():
            opts = {
                'use_https': self.request.is_secure(),
                'token_generator': default_token_generator,
                'from_email': settings.BUSINESS_EMAIL,
                'email_template_name': 'registration/password_reset_email_rest.html',
                'subject_template_name': 'registration/password_reset_subject.txt',
                'request': self.request,
                'html_email_template_name': 'registration/password_reset_email_rest.html',
                'extra_email_context': {
                    'base_url': base_url,
                    'business_name': settings.BUSINESS_NAME,
                },
            }
            form.save(**opts)

            return Response(
                {
                    "status": "success",
                    "message": _(
                        'If we know the email address you provided you will receive an email with a link to reset '
                        'your password. Please click on that link to follow through with the password reset.'
                    ),
                },
                status=status.HTTP_200_OK,
                headers=headers
            )
