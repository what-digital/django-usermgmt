from django.contrib.auth import get_user_model
from rest_framework.response import Response
from django.utils.translation import gettext as _

from ..serializers import UserSignupSerializer
from rest_framework import viewsets, permissions, mixins, status


class UserSignupViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    api endpoint that shows the current user
    you can use the GET parameter 'base_url_override' to specify the base url (protocol and domain) of the validation
    link in the email, please add a slash at the end of the url, example: ?base_url_override=https://www.domain.com/
    """
    serializer_class = UserSignupSerializer
    permission_classes = (permissions.AllowAny,)

    queryset = get_user_model().objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return Response({
            "status": "success",
            "message": _(
                'We have received your signup and have sent an email to you for email address validation. '
                'Please go to your inbox and click on the link to continue the signup process.'
            )
        },
            status=status.HTTP_201_CREATED,
            headers=headers
        )
