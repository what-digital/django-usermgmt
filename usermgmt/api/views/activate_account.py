from django.contrib.auth import get_user_model
from rest_framework.response import Response
from django.utils.translation import gettext as _
from usermgmt.models import UserValidationCode
from ..serializers import UserActivationSerializer
from rest_framework import viewsets, permissions, mixins, status


class UserActivationViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    api endpoint that activates a user account
    """

    queryset = UserValidationCode.objects.all()

    serializer_class = UserActivationSerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        """
        This is just to show a success message instead of the hash
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({
            "status": "success",
            "message": _("User has been activated"),
        },
            status=status.HTTP_200_OK,
            headers=headers
        )
