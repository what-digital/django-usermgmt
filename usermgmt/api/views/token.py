from ..serializers import UserSerializer
from rest_framework import response, viewsets, mixins, permissions
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from django.views.decorators.csrf import csrf_exempt


class ObtainAuthTokenViewSet(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    """
    gets or creates a DRF token for the username / password combination
    """
    serializer_class = AuthTokenSerializer
    permission_classes = (permissions.AllowAny,)

    # this will otherwise throw an error because the session middleware is kicking in by default
    @csrf_exempt
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        data = {
            'token': token.key,
            'user': UserSerializer(
                user, context={'request': request}
            ).data
        }

        return response.Response(data)
