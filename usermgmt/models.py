from urllib.parse import urlencode

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db import models
import uuid

from django.template.loader import get_template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ugettext

from django.contrib.auth import models as auth_models, get_user_model
from dry_rest_permissions.generics import allow_staff_or_superuser, authenticated_users

from django.contrib.auth.base_user import BaseUserManager
# Create your models here.


# I don' like this, would rather have this in the model class but heck...
# http://stackoverflow.com/questions/3573834/django-fields-default-value-from-self-models-instances
def generate_random_hash():
    return uuid.uuid1().hex


# from https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractbaseuser
class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(auth_models.AbstractUser):

    # make email unique
    email = models.EmailField(_('email address'),  unique=True,)
    username = None
    objects = UserManager()
    USERNAME_FIELD = 'email'

    # required for manage.py commands
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def clean(self):
        if self.email:
            self.email = self.email.lower().strip()

    def is_superuser_or_staff(self):
        return self.is_superuser or self.is_staff

    def send_validation_mail(self, base_url_override=None, next_url=None):
        # http://sheeshmohsin.wordpress.com/2014/02/18/send-html-template-email-using-django/
        code = UserValidationCode(user=self)
        code.save()

        plaintext = get_template('registration/email_validation_body.txt')
        htmly = get_template('registration/email_validation_body.html')

        base_url = settings.BASE_URL
        if base_url_override:
            base_url = base_url_override

        params = {
            'account_activation': 'true',
            'hash': code.hash,
        }

        if next_url:
            params['next'] = next_url

        query_string = urlencode(params)

        validation_url = "{}?{}".format(
            base_url,
            query_string,
        )

        d = {
            'validation_url': mark_safe(validation_url),
            'email': self.email,
        }

        subject = ugettext("validate your email address")
        from_email = settings.BUSINESS_EMAIL_VANE
        to = self.email
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    authentication_type = "user"

    # auth system has been patched so that request.user can also be an offer
    # (if OfferToken authentication is successful)
    # DRY Rest Permission implementation on all model will check the following
    # it should be "offer" or "user"

    # DRY Rest Permissions
    # http://pythonhosted.org/dry-rest-permissions/#setup

    @staticmethod
    @allow_staff_or_superuser
    def has_read_permission(request):
        if request.user.authentication_type == 'offer':
            return False
        return True

    @staticmethod
    @allow_staff_or_superuser
    def has_write_permission(request):
        return False

    @staticmethod
    @allow_staff_or_superuser
    def has_create_permission(request):
        return False

    @allow_staff_or_superuser
    def has_object_read_permission(self, request):
        if request.user.authentication_type == 'offer':
            return False
        # user needs to be added to members to see project
        return request.user.pk is self.pk

    @allow_staff_or_superuser
    def has_object_write_permission(self, request):
        return False


# This stores the hashes for email validation
class UserValidationCode(models.Model):
    created_date = models.DateTimeField('date published', auto_now_add=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    # The hash is unique and auto-generated for an existing user
    hash = models.CharField(max_length=200, blank=True, default=generate_random_hash, editable=False, unique=True)

    def get_user_email(self):
        return self.user.email

    def __str__(self):
        return self.user.email
